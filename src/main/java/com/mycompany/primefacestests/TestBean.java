package com.mycompany.primefacestests;

import java.io.Serializable;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class TestBean implements Serializable{

  public TestBean() {
    }
  
   private Date cal;

    public Date getCal() {
        return cal;
    }

    public void setCal(Date cal) {
        this.cal = cal;
    }
    private int count = 0;
 
    public void increment(){
        this.count += 10;
    }
    public void submit(){
    }
    public void setCount(int count) {
        this.count = count;
    }
    public int getCount() {
        return count;
    }
    
  
    
}
