package com.mycompany.primefacestests;

import org.primefaces.event.CellEditEvent;
import org.primefaces.event.TabChangeEvent;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewExpiredException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@ManagedBean
@ViewScoped
public class TableBean implements Serializable
{

    private final static String[] colors;

    private final static String[] manufacturers;
    private String version;
    private String data;

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    private List<Car> carsSmall;

    static
    {
        colors = new String[10];
        colors[0] = "Black";
        colors[1] = "White";
        colors[2] = "Green";
        colors[3] = "Red";
        colors[4] = "Blue";
        colors[5] = "Orange";
        colors[6] = "Silver";
        colors[7] = "Yellow";
        colors[8] = "Brown";
        colors[9] = "Maroon";

        manufacturers = new String[10];
        manufacturers[0] = "Mercedes";
        manufacturers[1] = "BMW";
        manufacturers[2] = "Volvo";
        manufacturers[3] = "Audi";
        manufacturers[4] = "Renault";
        manufacturers[5] = "Opel";
        manufacturers[6] = "Volkswagen";
        manufacturers[7] = "Chrysler";
        manufacturers[8] = "Ferrari";
        manufacturers[9] = "Ford";
    }

    public TableBean()
    {
        carsSmall = new ArrayList<Car>();

        populateRandomCars(carsSmall, 9);

        version = FacesContext.class.getPackage().getImplementationTitle() + " " + FacesContext.class.getPackage().getImplementationVersion();
    }

    private void populateRandomCars(List<Car> list, int size)
    {
        for (int i = 0; i < size; i++)
        {
            list.add(new Car(getRandomModel(), getRandomYear(), getRandomManufacturer(), getRandomColor()));
        }
    }

    public List<Car> getCarsSmall()
    {
        return carsSmall;
    }

    private int getRandomYear()
    {
        return (int) (Math.random() * 50 + 1960);
    }

    private String getRandomColor()
    {
        return colors[(int) (Math.random() * 10)];
    }

    private String getRandomManufacturer()
    {
        return manufacturers[(int) (Math.random() * 10)];
    }

    private String getRandomModel()
    {
        return UUID.randomUUID().toString().substring(0, 8);
    }

    public String[] getManufacturers()
    {
        return manufacturers;
    }

    public String[] getColors()
    {
        return colors;
    }

    public void onCellEdit(CellEditEvent event)
    {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue))
        {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onTabChange(TabChangeEvent event)
    {
        //Save update your file here for current datatable.
        //update entries to now have the new table header.
        System.out.println("onTabChange() new Tab:" + (String) event.getData());
    }
    
    public void submitListener(AjaxBehaviorEvent event)
    {
        submitListener();
    }

    public void submitListener()
    {
        throw new NullPointerException("fake nullathon");

    }

    public String doAction()
    {
        return "productdetails";
    }
            
    public String form2Submit()
    {
        throw new ViewExpiredException("This is simulated ViewExpiredException", FacesContext.getCurrentInstance().getViewRoot().getViewId());
    }
    
    private Date dt1;
    private Date dt2;
    public Date getDt1()
    {
        return dt1;
    }

    public void setDt1(Date dt1)
    {
        this.dt1 = dt1;
    }
     public Date getDt2()
    {
        return dt2;
    }

    public void setDt2(Date dt2)
    {
        this.dt2 = dt2;
    }

    
}
