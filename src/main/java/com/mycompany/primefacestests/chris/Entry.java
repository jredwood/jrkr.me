package com.mycompany.primefacestests.chris;

import org.codehaus.jackson.annotate.JsonProperty;

public class Entry
{
    @JsonProperty(value = "Entry")
    private String entry;
    @JsonProperty(value = "Value")
    private String value;

    public Entry()
    {

    }

    public Entry(String entry, String value)
    {
        this.entry = entry;
        this.value = value;
    }

    public String getEntry()
    {
        return entry;
    }

    public void setEntry(String entry)
    {
        this.entry = entry;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}
