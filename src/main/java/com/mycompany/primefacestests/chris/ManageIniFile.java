package com.mycompany.primefacestests.chris;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.CellEditEvent;
import org.primefaces.event.TabChangeEvent;

@ManagedBean
@RequestScoped
public class ManageIniFile
{
    @ManagedProperty(value = "#{iniFile}")
    private IniFile iniFileBean;

    public IniFile getIniFileBean()
    {
        return iniFileBean;
    }

    public void setIniFileBean(IniFile iniFileBean)
    {
        this.iniFileBean = iniFileBean;
    }

    public void onTabChange(TabChangeEvent event)
    {
        //Save update your file here for current datatable.
        //update entries to now have the new table header.
        iniFileBean.saveData();
        iniFileBean.updateSectionData((String) event.getData());
        System.out.println("onTabChange() new Tab:" + (String) event.getData());
    }
    public void destroyApplicationBean() 
    {
        FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().remove("iniFile");
    }
    public void onCellEdit(CellEditEvent event)
    {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        System.out.println("oldValue             = " + oldValue);
        System.out.println("newValue             = " + newValue);

        if (newValue != null && !newValue.equals(oldValue))
        {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else
        {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell UnChanged", "Que?");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

}
