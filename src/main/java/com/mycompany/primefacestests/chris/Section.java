package com.mycompany.primefacestests.chris;

import java.util.List;

public class Section
{
    private String id;
    private List<Entry> entries;

    public List<Entry> getEntries()
    {
        return entries;
    }

    public void setEntries(List<Entry> entries)
    {
        this.entries = entries;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

}
