package com.mycompany.primefacestests.chris;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;

@ManagedBean(name = "iniFile")
@ApplicationScoped
public class IniFile
{
    private Data data;
    private List<String> sections;
    private Section openSection;
    
    public static ObjectMapper mapper = new ObjectMapper();
    public URL url;

    public IniFile()
    {
        url = getClass().getResource("/SectionEntries.json");
        try
        {
            data = mapper.readValue(url, Data.class);
        } catch (IOException ioe)
        {
            throw new RuntimeException("DataMapper from url file location not found", ioe);
        }
        
        initSections(); 
        openSection = data.getSections().get(0);
        // This is only a REFERENCE, so any data modified on the open tab, is actually modifying the data object.
    }
    private void initSections() {
        List<String> secvt = new ArrayList(1);
        for (Section sec : data.getSections())
        {
            secvt.add(sec.getId());
        }
        
        sections = secvt;
    }
   public List<String> getSections()
    {
        return sections;
    }
    public void saveData()
    {
         try
        {
            mapper.writeValue(new File(url.getPath()), data);
        } catch (IOException ioe)
        {
           FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Save Failed", "");
           FacesContext.getCurrentInstance().addMessage(null, msg);
           System.out.println("IOException:saveData() + " + ioe);
           return;
        }
         
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Successful", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        //TODO - Store this file somewhere other than the server (MYSQL etc, otherwise it resets to my standard file with no updates)
    }

    
    public void updateSectionData(String section)
    {
        for (Section sec : data.getSections())
        {
            if (StringUtils.endsWithIgnoreCase(section, sec.getId()))
            {
                openSection =  sec;
                return;
            }
        }
        
        throw new RuntimeException("Section with id: " + section + "not found!");
    }

    public Data getData()
    {
        return data;
    }

    public void setData(Data data)
    {
        this.data = data;
    }

    public Section getOpenSection()
    {
        return openSection;
    }

    public void setOpenSection(Section openSection)
    {
        this.openSection = openSection;
    }

}
